User.create!([
  {first_name: "admin", last_name: "admin", email: "admin@gmail.com", phone: "8417891491", password: "password", gender: "female", is_admin: true},
  {first_name: "user", last_name: "user", email: "user@gmail.com", phone: "8417234234", password: "password", gender: "male", is_admin: nil},
  {first_name: "user2", last_name: "user2", email: "user2@gmail.com", phone: "929292929292", password: "password", gender: "female", is_admin: nil},
  {first_name: "admin2", last_name: "admin2", email: "admin2@gmail.com", phone: "9999999999", password: "password", gender: "female", is_admin: true}
])
