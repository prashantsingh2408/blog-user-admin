
Rails.application.routes.draw do
  root to: 'auth#index'
  #auth
  #signin and signup 
  get '/signin', to: 'auth#index'
  get '/login', to: 'auth#index'

  get '/signup', to: 'auth#index'
  get '/register', to: 'auth#index'

  post '/signin', to: 'auth#signin'
  post '/login', to: 'auth#signin'

  post '/signup', to: 'auth#signup'
  post '/register', to: 'auth#signup'

  delete '/signout', to: 'auth#signout', as: 'logout' 
  delete '/logout', to: 'auth#signout'
  
  #index -> show blog list
  #edit -> edit user detail (from admin)
  #update ->  update password (from admin)
  #destroy -> delete user(from admin)
  resources :users, only: [:index, :edit, :update, :destroy]
 
  #index -> show blog list
  #show -> show blog detail
  #create -> create new blog
  #update -> update blog detail
  resources :user_articles, only: [:index, :show, :create,:update, :destroy]
  
  get 'admins' => 'admins#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
