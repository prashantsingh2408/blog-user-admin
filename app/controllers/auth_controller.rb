class AuthController < ApplicationController

  def index

  end

  def signup
   logger.debug params[:male].present?
  #  byebug
   User.create(
     first_name: params[:first_name],
     last_name: params[:last_name],
     email: params[:email],
     phone: params[:phone],
     password: params[:password],
     gender: params[:gender],
     is_admin: params[:is_admin])    
    flash[:notice] = "You are registered,Login to continue"
    render action: "index"
  end

  def signin
    # log
    # logger.debug params[:email] 
    # logger.debug params[:password]
    # byebug
    if user = User.find_by(email: params[:email].downcase)
                  .authenticate(params[:password])
      session[:login_user] = user.email
      session[:user_id] = user.id
      if user.is_admin
        session[:is_admin] = true
        flash[:notice] = "Welcome #{user.first_name}!"
        users = User.all
        redirect_to admins_path
      else
        @user = User.find_by(email: params[:email].downcase)
        flash[:notice] = "Welcome #{user.first_name}!"
        redirect_to user_articles_path
      end
    else
      flash[:notice] = "Invalid email or password"
      render action: "index"
    end
  end
  def signout
    session[:user_id] = nil
    flash[:notice] = "You have logged out"
  end

  
    
end
