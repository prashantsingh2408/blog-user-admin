class UsersController < ApplicationController

  def index
  end
  #for admin
  def update
    logger.debug params['user'] 
    # byebug
    @user = User.find(params[:id])
    if params[:user][:first_name].present?
      @user.update(
                  first_name: params[:user][:first_name],
                  last_name: params[:user][:last_name],
                  email: params[:user][:email],
                  phone: params[:user][:phone],
      )
    else params[:user][:password].present?
      if params[:user][:password] == params[:user][:password_confirmation]
        @user.update(
                    password: params[:user][:password],
        )
     else
        flash[:error] = "Password and Confirm Password does not match"
      end
    end
    redirect_to admins_path
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = @user.first_name + " User deleted"
    redirect_to admins_path
  end
end
