class UserArticlesController < ApplicationController
  def new
    respond_to do |format|
      format.html
      format.js
    end
  end 
  def index 
    render HTML: "article"
  end
  def show  
    render HTML: "article"
  end

  def create 
    UserArticle.update(
            user_id: params[:user_id],
            title: params[:title], 
            content: params[:content]
          )
   end
   
   def destroy
      UserArticle.destroy(params[:id])
    end
end
